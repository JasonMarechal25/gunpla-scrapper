import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SiteComponent} from './site/site.component';
import {MatTableModule} from '@angular/material';
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {SitesRepositoryService} from "./site/sites.repository.service";
import {MatGridListModule} from "@angular/material/grid-list";
import {ComparateurComponent} from "./comparateur/comparateur.component";

@NgModule({
  declarations: [
    AppComponent,
    SiteComponent,
    ComparateurComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([]),
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule
  ],
  providers: [
    SitesRepositoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
