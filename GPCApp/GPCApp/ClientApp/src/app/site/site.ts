﻿import {Product} from "./product";

export interface Site {
  siteName: string;
  siteKey: string;
  siteURL: string;
  products: Product[];
}
