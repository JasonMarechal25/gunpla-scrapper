import {Component, Inject, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatTableDataSource} from "@angular/material/table";
import {Product} from "./product";
import {Site} from "./site";

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['site.component.css']
})
export class SiteComponent implements OnInit, OnChanges {
  @Input() site: Site;
  @Input() filter: string;
  public productsDataSource: MatTableDataSource<Product>;


  displayedColumns = [ 'name', 'price', 'devise']

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.productsDataSource = new MatTableDataSource<Product>()
  }

  ngOnInit(): void {
    this.productsDataSource = new MatTableDataSource<Product>(this.site.products);
    this.productsDataSource.filterPredicate = (data: Product, filter: string) => {
      const trimmedValue = filter.trim();
      return data.name.includes(trimmedValue) ||
        data.name.includes(trimmedValue.toLocaleLowerCase()) ||
        data.name.includes(trimmedValue.toLocaleUpperCase());
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.productsDataSource.filter = this.filter;
  }
}

