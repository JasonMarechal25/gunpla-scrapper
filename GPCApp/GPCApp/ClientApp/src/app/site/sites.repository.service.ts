﻿import {Site} from "./site";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class SitesRepositoryService {
  sites: Site[] = [];

  constructor() {
  }

  public updateSites(sites: Site[]) {
    this.sites = sites;
  }
}
