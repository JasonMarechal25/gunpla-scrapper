﻿import {SitesRepositoryService} from "../site/sites.repository.service";
import {Component} from "@angular/core";

@Component({
  selector: 'app-comparateur',
  templateUrl: './comparateur.component.html'
})
export class ComparateurComponent {

  filterValue = ""
  constructor(public sitesRepositoryService: SitesRepositoryService) {
  }

  public applyFilter(value: string) {
    this.filterValue = value;
  }
}
