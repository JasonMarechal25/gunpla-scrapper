import {Component, Inject, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {HttpClient} from "@angular/common/http";
import {Site} from "./site/site";
import {SitesRepositoryService} from "./site/sites.repository.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string,
              public sitesRepositoryService: SitesRepositoryService) {

  }

  ngOnInit(): void {
    this.http.get<Site[]>(this.baseUrl + 'sites').subscribe(result =>
    {
      console.log(result);
      this.sitesRepositoryService.updateSites(result);
    }, error => console.error(error));
  }
}
