using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using static System.Text.Json.JsonSerializer;

namespace Loader
{
    [Controller]
    public class ProductLoader : IDisposable
    {
        private readonly ISitesRepository _sitesRepository;
        private ProcessStartInfo Start { get; }


        public ProductLoader(ISitesRepository sitesRepository)
        {
            _sitesRepository = sitesRepository;
            Start = new ProcessStartInfo
            {
                FileName = "python",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                WorkingDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) ?? ".",
            };
        }
        public void LoadProducts(string scrapersRoot)
        {
            //Dans les scraper ajouter des info "Nom, adresse, ..."
            var RoGSite = LoadSite(scrapersRoot, "RiseOfGunpla");
            var GunplaShopSite = LoadSite(scrapersRoot, "GunplaShop");
            var GundamWiki = LoadSite(scrapersRoot, "GundamWiki");
            _sitesRepository.AddSite(RoGSite);
            _sitesRepository.AddSite(GunplaShopSite);
            _sitesRepository.AddSite(GundamWiki);
        }

        public Site LoadSite(string scrapersRoot, string siteFolder)
        {
            Start.Arguments = Path.Combine(scrapersRoot, "Scrapers", siteFolder, "scraper.py");
            var process = Process.Start(Start);
            var reader = process.StandardOutput;
            var result = reader.ReadToEnd();
            process.WaitForExit();
            var res = Deserialize<Site>(result);
            return res;
        }

        public void Dispose()
        {
            
        }
    }
}