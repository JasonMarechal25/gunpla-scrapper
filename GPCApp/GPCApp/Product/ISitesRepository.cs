﻿using System.Collections.Generic;

namespace Loader
{
    public interface ISitesRepository
    {
        IList<Site> Sites { get; set; }
        void AddSite(Site site);
    }
}