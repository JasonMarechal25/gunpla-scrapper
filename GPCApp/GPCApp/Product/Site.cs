﻿using System;
using System.Collections.Generic;

namespace Loader
{
    public class Site
    {
        public string SiteName { get; set; }
        public string SiteKey { get; set; }
        public string SiteURL { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}