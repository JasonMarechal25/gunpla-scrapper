﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loader;

namespace Loader
{
    public class SitesRepository : ISitesRepository
    {
        public SitesRepository()
        {
            Sites = new List<Site>(10);
        }

        public IList<Site> Sites { get; set; }
        public void AddSite(Site site)
        {
            Sites.Add(site);
        }
    }
}