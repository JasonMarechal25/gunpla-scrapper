﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loader;
using Microsoft.AspNetCore.Mvc;

namespace GPCApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SitesController : ControllerBase
    {
        private readonly ISitesRepository _sitesRepository;

        public SitesController(ISitesRepository sitesRepository)
        {
            _sitesRepository = sitesRepository;
        }

        // GET
        [HttpGet]
        public IEnumerable<Site> Get()
        {
            return _sitesRepository.Sites;
        }
    }
}