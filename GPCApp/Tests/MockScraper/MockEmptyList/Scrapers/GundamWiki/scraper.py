﻿import json


def scrapSite():
    return json.dumps(dict(Products= [],
                      SiteName="GundamWiki",
                      SiteKey="GWIKI",
                      SiteURL="https://gundam.fandom.com/wiki/"));

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())