﻿import json


def scrapSite():
    return json.dumps(dict(Products= [],
                      SiteName="Gunpla Shop",
                      SiteKey="GSHOP",
                      SiteURL="https://www.gunpla-shop.com/"));

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())