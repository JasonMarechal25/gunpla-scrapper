﻿import json


def scrapSite():
    return json.dumps(dict(Products= [],
                      SiteName="Rise of Gunpla",
                      SiteKey="ROG",
                      SiteURL="https://riseofgunpla.com/"));

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())