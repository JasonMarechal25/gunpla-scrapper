﻿import json

allProducts = dict(Products=[{"Name": "PG UNLEASHED RX-78-2 GUNDAM", "Price": 280.00, "Devise": "\u20ac"},
                             {"Name": "PG 1/60 ARMED ARMOR DE TAIL STABILIZERS PHENEX (EXPANSION SET)",
                              "Price": 135.00, "Devise": "\u20ac"},
                             {"Name": "PG 1/60 UNICORN GUNDAM 03 PHENEX (NARRATIVE VER.)", "Price": 699.00,
                              "Devise": "\u20ac"},
                             {"Name": "PG 1/60 UNICORN GUNDAM \u2013 FINAL BATTLE VER.", "Price": 350.00,
                              "Devise": "\u20ac"},
                             {"Name": "PG 1/60 PERFECT STRIKE GUNDAM", "Price": 280.00, "Devise": "\u20ac"},
                             {"Name": "PG 1/60 EVANGELION", "Price": 150.00, "Devise": "\u20ac"},
                             {"Name": "LED UNIT POUR PG GUNDAM EXIA", "Price": 150.00, "Devise": "\u20ac"},
                             {"Name": "PG 1/60 RX-0 UNICORN GUNDAM LED UNIT", "Price": 140.00, "Devise": "\u20ac"},
                             {"Name": "PG 1/60 00 GUNDAM SEVEN SWORD/G", "Price": 280.00, "Devise": "\u20ac"},
                             {"Name": "PG 1/60 MBF-P02KAI GUNDAM ASTRAY RED FRAME KAI", "Price": 350.00,
                              "Devise": "\u20ac"}],
                   SiteName="Rise of Gunpla",
                   SiteKey="ROG",
                   SiteURL="https://riseofgunpla.com/");
def scrapSite():
    return json.dumps(allProducts)

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())