using System.Collections.Generic;
using System.IO;
using System.Linq;
using Loader;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Testscrapers
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestLoadEmpty()
        {
            var repo = new SitesRepository();
            var productLoader = new ProductLoader(repo);
            var site = productLoader.LoadSite($"{TestContext.CurrentContext.TestDirectory}/MockScraper/MockEmptyList", "RiseOfGunpla");
            Assert.AreEqual(site.Products.Count(), 0);
        }
        
        [Test]
        public void TestLoadAll()
        {
            var repo = new SitesRepository();
            var productLoader = new ProductLoader(repo);
            productLoader.LoadProducts($"{TestContext.CurrentContext.TestDirectory}/MockScraper/MockFullList");
            Assert.Greater(repo.Sites.Count(), 0);
        }
        
        [Test]
        public void TestLoadSite()
        {
            var repo = new SitesRepository();
            var productLoader = new ProductLoader(repo);
            var res = productLoader.LoadSite($"{TestContext.CurrentContext.TestDirectory}/MockScraper/MockFullList", "RiseOfGunpla");
            Assert.AreEqual(res.SiteKey, "ROG");
            Assert.AreEqual(res.SiteName, "Rise of Gunpla");
            Assert.AreEqual(res.SiteURL, "https://riseofgunpla.com/");
            Assert.Greater(res.Products.Count(), 0);
        }
    }
}