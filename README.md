# Gunpla Price Checker

A simple price comparator for various gunpla online shop.

Initially meant to help me compare product price when in a shop and I want to compare the price tag to what it would cost if bought online. Target french reseller as I am living in france.

# Product vision

I'd like the site to present price comparaison for the main french online shop like Rise of Gunpla and kind of a "reference price" like Bandai Hobby Store (which doesn't have an online listing) but also to present results from Amazon, Ebay and Leboncoin.

#Technical description

The project is split in 4 parts

* Front-end in Angular. What users see
* Backend in C#. As simple as possible, only route data to front-end. In the fututr may host various feature like caching
* Scrappers in python. Scrap web site to build data on demand. Ieally hot swapable and reloadable at runtime by the backend allowing fixes, removing or adding scrappers without pausing the service
* Docker image. For now the image to properly build a package for the CI.

##Goals

For each part here is my vision:

* Front-end: Continue to build a light and accessible front-end.
** Light because hobby shop are badly serviced from experience for various reasons (thick walls, underground, lot's of people, ...)
** Accessible even if it means changing technology or design. It's just a matter of principle.
* Back-end: Hold features like caching, target (online shop) update and so on
* Scrapers: implement more sites. Use API if possible (Amazon ?), not necessary in python as long the output match the expectation of the backend (a json)
* Images and by extension infrastructures. Better backup running environment to be able to recover and redeploy a sane environment in case of curruption.
