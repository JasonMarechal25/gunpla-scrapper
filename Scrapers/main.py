# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import requests
from bs4 import BeautifulSoup


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
    URL = 'https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg/page/'

    pageCounter = 1
    page = requests.get(URL + str(pageCounter))
    items = []
    while page.ok:
        soup = BeautifulSoup(page.content, 'html.parser')
        products = soup.find_all('li', class_='type-product')
        for p in products:
            name = p.find('h2', class_='woocommerce-loop-product__title').get_text()
            price = p.find('span', class_='amount').next_element
            devise = p.find('span', class_='woocommerce-Price-currencySymbol').get_text()

            items.append({
                "name": name,
                "price": price,
                "devise": devise
            })
        pageCounter += 1
        page = requests.get(URL + str(pageCounter))
    print(items)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
