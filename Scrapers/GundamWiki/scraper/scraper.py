﻿import requests
from bs4 import BeautifulSoup
import json

SiteName = "GundamWiki"
SiteUrl = "https://gundam.fandom.com/wiki/"
SiteKey = "GWIKI"

def scrap(URL):
    page = requests.get(URL)
    if not page.ok:
        data = {"Products": []}
        return json.dumps(data)
    items = []
    soup = BeautifulSoup(page.content, 'html.parser')
    productTable = soup.find('table', class_='wikitable')
    tableBody = productTable.find('tbody')
    productLines = tableBody.find_all('tr')
    productLines.pop(0)
    for productLine in productLines:
        columns = productLine.find_all('td')
        nameItem = columns[0]
        name = 'PG-' + nameItem.next_element + ' ' + columns[1].next_element.next_element
        price = round(float(columns[3].next_element.replace(',','').replace("¥", '').replace('\n', '')), 2)
        devise = "¥"

        items.append({
            "Name": name,
            "Price": price,
            "Devise": devise
        })

    data = {"Products": items}
    return json.dumps(data)


def scrapAll(URL):
    allProducts = dict(Products=[], SiteName=SiteName, SiteURL=SiteUrl, SiteKey=SiteKey)
    allProducts["Products"] = json.loads(scrap(URL))["Products"]
    return json.dumps(allProducts)


def scrapSite():
    return scrapAll('https://gundam.fandom.com/wiki/Perfect_Grade')

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())
