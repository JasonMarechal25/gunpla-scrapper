import json
import unittest
import GundamWiki.scraper.scraper as scrapper

class MyTestCase(unittest.TestCase):
    def test_ScapSite(self):
        result = scrapper.scrapAll("https://gundam.fandom.com/wiki/Perfect_Grade")
        try:
            dict_result = json.loads(result)
            self.assertTrue(len(dict_result["Products"]) > 0)
        except Exception as err:
            self.assertFalse(True, "Json not valid")

    def test_MatchFormat(self):
        result = json.loads(scrapper.scrapAll("https://gundam.fandom.com/wiki/Perfect_Grade"))
        self.assertTrue("SiteName" in result)
        self.assertTrue("SiteURL" in result)
        self.assertTrue("Products" in result)
        self.assertTrue(isinstance(result["SiteName"], str))
        self.assertTrue(isinstance(result["SiteURL"], str))
        self.assertTrue(isinstance(result["Products"], list))
        self.assertTrue(isinstance(result["SiteKey"], str))

if __name__ == '__main__':
    unittest.main()
