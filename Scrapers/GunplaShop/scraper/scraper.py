﻿import urllib.request
import urllib.parse
import urllib.error
from bs4 import BeautifulSoup
import json
import re

SiteName = "Gunpla Shop"
SiteUrl = "https://www.gunpla-shop.com/"
SiteKey = "GSHOP"

in_stock_option = '/d/Disponibilité=>0'

def scrap(URL, pageCounter):
    req = urllib.request.Request(URL+urllib.parse.quote(in_stock_option)+'/'+str(pageCounter), headers={'User-Agent' : ""})
    try:
        resp = urllib.request.urlopen(req,timeout=5)
        html = resp.read().decode()
    except urllib.error.HTTPError:
        data = {"Products": []}
        return json.dumps(data)
    if not resp.status == 200:
        data = {"Products": []}
        return json.dumps(data)
    items = []
    soup = BeautifulSoup(html, 'html.parser')
    noProduct = soup.find('p', "prod__noResult")
    if (noProduct):
        return json.dumps({"Products": []})
    products = soup.find_all('div', class_='prod__relative')
    regex = re.compile(r"^BANDAI GUN.* GUNPLA ")
    for p in products:
        name = p.find('p', class_='prod__name').get_text().replace('BANDAI GUNPLA ', '')
        name = regex.sub('', name)
        priceElement = p.find('p', 'prod__price__cur')
        price = float(priceElement.find('strong').next_element.replace(',','.'))
        devise = str(priceElement.find('span', 'prod__price__symb').contents[0])

        items.append({
            "Name": name,
            "Price": price,
            "Devise": devise
        })
    data = {"Products": items}
    return json.dumps(data)


def scrapAll(URL):
    pageCounter = 1
    allProducts = dict(Products=[], SiteName=SiteName, SiteURL=SiteUrl, SiteKey=SiteKey)
    products = json.loads(scrap(URL, pageCounter))
    while len(products["Products"]) > 0:
        allProducts["Products"] += products["Products"]
        pageCounter += 1
        products = json.loads(scrap(URL, pageCounter))
    return json.dumps(allProducts)


def scrapSite():
    return scrapAll('https://www.gunpla-shop.com/gunpla-1-60-48/perfect-grade-pg')

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())
