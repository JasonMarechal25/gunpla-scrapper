import json
import unittest
import GunplaShop.scraper.scraper as scrapper

class MyTestCase(unittest.TestCase):
    def test_ScapMultiplePage(self):
        return
        base = scrapper.scrap("https://www.gunpla-shop.com/gunpla-1-60-48/perfect-grade-pg/")
        result = scrapper.scrapAll("https://www.gunpla-shop.com/gunpla-1-60-48/perfect-grade-pg/")
        try:
            dict_base = json.loads(base)
            dict_result = json.loads(result)
            self.assertTrue(len(dict_result["Products"]) > len(dict_base["Products"]))
        except Exception as err:
            self.assertFalse(True, "Json not valid")

    def test_MatchFormat(self):
        result = json.loads(scrapper.scrapAll("https://www.gunpla-shop.com/gunpla-1-60-48/perfect-grade-pg/"))
        self.assertTrue("SiteName" in result)
        self.assertTrue("SiteURL" in result)
        self.assertTrue("Products" in result)
        self.assertTrue(isinstance(result["SiteName"], str))
        self.assertTrue(isinstance(result["SiteURL"], str))
        self.assertTrue(isinstance(result["Products"], list))
        self.assertTrue(isinstance(result["SiteKey"], str))

if __name__ == '__main__':
    unittest.main()
