﻿import requests
from bs4 import BeautifulSoup
import json

SiteName = "Rise of Gunpla"
SiteUrl = "https://riseofgunpla.com/"
SiteKey = "ROG"

in_stock_option = '?mta__stock_status_2_10=instock'

def scrap(URL):
    page = requests.get(URL+in_stock_option)
    if not page.ok:
        data = {"Products": []}
        return json.dumps(data)
    items = []
    soup = BeautifulSoup(page.content, 'html.parser')
    products = soup.find_all('li', class_='type-product')
    for p in products:
        name = p.find('h2', class_='woocommerce-loop-product__title').get_text()
        price = float(p.find('span', class_='amount').next_element.next_element.replace('.','').replace(',','.')) #Span->Bdi->Prix
        devise = p.find('span', class_='woocommerce-Price-currencySymbol').get_text()

        items.append({
            "Name": name,
            "Price": price,
            "Devise": devise
        })
    data = {"Products": items}
    return json.dumps(data)


def scrapAll(URL):
    pageCounter = 1
    allProducts = dict(Products=[], SiteName=SiteName, SiteURL=SiteUrl, SiteKey=SiteKey)
    products = json.loads(scrap(URL + '/page/' + str(pageCounter)))
    while len(products["Products"]) > 0:
        allProducts["Products"] += products["Products"]
        pageCounter += 1
        products = json.loads(scrap(URL + '/page/' + str(pageCounter)))
    return json.dumps(allProducts)


def scrapSite():
    return scrapAll('https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg')

if __name__ == "__main__":
    # execute only if run as a script
    print(scrapSite())
