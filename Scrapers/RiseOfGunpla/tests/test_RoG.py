import unittest
import RiseOfGunpla.scraper.scraper as scrapper
import json

class Scrapper(unittest.TestCase):
    def test_ListFilled(self):
        result = scrapper.scrap("https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg")
        self.assertTrue("Products" in result)

    def test_EmptyOnError(self):
        result = scrapper.scrap("https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg_bad")
        try:
            dict_result = json.loads(result)
            self.assertTrue(not dict_result["Products"])
        except Exception as err:
            self.assertFalse(True, "Json not valid")

    def test_ScapMultiplePage(self):
        return #With "in stock" option, we don't have several pages anymore for PG. May activate again for other products
        base = scrapper.scrap("https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg/")
        result = scrapper.scrapAll("https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg/")
        try:
            dict_base = json.loads(base)
            dict_result = json.loads(result)
            self.assertTrue(len(dict_result["Products"]) > len(dict_base["Products"]))
        except Exception as err:
            self.assertFalse(True, "Json not valid")

    def test_MatchFormat(self):
        result = json.loads(scrapper.scrapAll("https://riseofgunpla.com/categorie-produit/gunpla/perfect-grade-pg"))
        self.assertTrue("SiteName" in result)
        self.assertTrue("SiteURL" in result)
        self.assertTrue("Products" in result)
        self.assertTrue(isinstance(result["SiteName"], str))
        self.assertTrue(isinstance(result["SiteURL"], str))
        self.assertTrue(isinstance(result["Products"], list))
        self.assertTrue(isinstance(result["SiteKey"], str))

if __name__ == '__main__':
    unittest.main()
